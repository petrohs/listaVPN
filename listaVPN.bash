#!/bin/bash
ayuda () { echo '
#===============================================================================
#        SCRIPT:  listaVPN.bash
# 
#           USO:  listaVPN.bash [-h]
#   DESCRIPCION:  Identifica si esta conectado a la vpn
#                 Si no esta conectado muestra lista de vpns y conecta
#                 Si esta conectado pregunta si desconecta
#      OPCIONES:  -h  ayuda
#  DEPENDENCIAS:  vpnc, gksudo, zenity
#          BUGS:  ---
#         NOTAS:  Es necesario configurar la variable $BASE al dir con perfiles
#           URL:  https://github.com/petrohs/listaVPN
#     VERSIONES:  20160625 0.0.1 PetrOHS Inicial
#===============================================================================
' | more;} 

#ayuda
 if [ "$1" = "-h" -o "$1" = "--help" -o "$1" = "--ayuda" ]
   then ayuda; exit 1; fi

#Ruta donde estan los perfiles
 BASE="/tmp/listaVPN/perfiles";

#identificando si esta ejecutandose vpnc, de encontrase se obtiene el perfil con el que se conecto
 REGISTRO=`ps -fea | grep vpnc | grep -v grep | awk '{print $9}'`
 if [ -z "${REGISTRO}" ]
   then  #no conectado
     cd $BASE  #entrar al directorio y obtener lista de perfiles
     PERFIL=`ls -1 | zenity --list --title="VPNs" --text="No hay VPN en ejecución\n¿Quieres conectar con un perfil?" --column Perfiles 2>/dev/null`
     if [ -n "${PERFIL}" ]
       then  #cuando seleccionan un perfil, conecta vpnc via gksudo
         gksudo -m "Ejecutar 'vpnc-connect $PERFIL'" "vpnc-connect ${BASE}/${PERFIL}" >/tmp/logVPNohs
         zenity --text-info --title "Resultado de la conexión" --filename=/tmp/logVPNohs 2>/dev/null
         if [ $? -ne 0 ]
           then   #si falla lanza una terminal para validar manualmente
              xterm &
           fi
       fi
   else  #si conectado
     zenity --question --title="VPNs" --text="Esta conectado con perfil `basename $REGISTRO`\n¿Quiere detenerlo?" 2>/dev/null
     if [ $? -eq 0 ]
       then  #si quiere desconectar ejecuta vpnc via gksudo
         gksudo -m "Ejecutar 'vpnc-disconnect $PERFIL'" "vpnc-disconnect" >/tmp/logVPNohs
         zenity --text-info --title "Resultado de la desconexión" --filename=/tmp/logVPNohs 2>/dev/null
         if [ $? -ne 0 ]
           then  #si falla lanza una terminal para validar manualmente
              xterm &
           fi
       fi
   fi

#"Hombres haga quien quiera hacer pueblos" Jose Marti
 exit 0;
